$(document).ready(function(){
	$('.sidenav').sidenav({
		edge: 'right'
	});
	$('select').formSelect();
	$('.collapsible').collapsible({
		accordion: false
	});
	$('.modal').modal();
	$('.faq-wrap').scrollbar();
	$('.table-wrap').scrollbar();
	$('.my-prizes').scrollbar();
	$('.input-wrap textarea').scrollbar();
	$('.dropdown-content.select-dropdown').each(function(){
		$(this).find('li').wrapAll("<div class='li-wrap'></div>");
	});
	$('.li-wrap').scrollbar();
	$('.bg-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.products-slider'
	});
	$('.products-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		asNavFor: '.bg-slider',
		dots: true,
		centerMode: true,
		focusOnSelect: true,
		fade: true
	});
	$('.mobile-lk-wrap').slick({
		dots: false,
		arrows: false,
		infinite: false,
		speed: 300,
		slidesToShow: 1,
		variableWidth: true
	});
	$('.dropify').dropify({
	    tpl: {
	        clearButton: '<button type="button" class="dropify-clear"><img src="img/delete.png"></button>'
	    }
	});
	$(".phone-mask").inputmask({
        mask:"+7 (999) 999-99-99",
        "clearIncomplete": true
    });

    // function urur() {
    //     var arr=[];
    //     arr[0]='brazil';
    //     arr[1]='cuba';
    //     arr[2]='iceland';
    //     arr[3]='japan';
    //     arr[4]='kenya';
    //     arr[5]='nepal';
    //     arr[6]='thailand';
    //     var i=Math.floor(Math.random()*arr.length);
    //     $('.page-prizes').addClass(arr[i]);
    // }
    // urur();

	function timeoutCountry() {
		var i = 1;
		var arr_bg=[];
	        arr_bg[0]='brazil';
	        arr_bg[1]='iceland';
	        arr_bg[2]='cuba';
	        arr_bg[3]='nepal';
	        arr_bg[4]='japan';
	        arr_bg[5]='thailand';
	        arr_bg[6]='kenya';
	    var arr_country=[];
	        arr_country[0]='halls-brazil';
	        arr_country[1]='halls-iceland';
	        arr_country[2]='halls-cuba';
	        arr_country[3]='halls-nepal';
	        arr_country[4]='halls-japan';
	        arr_country[5]='halls-thailand';
	        arr_country[6]='halls-kenya';
		setInterval(function() {
			if(i%2==0 && i!=0) {
				$('.first-bg').delay(1000).removeClass('brazil iceland cuba nepal japan thailand kenya');
				$('.subfirst-bg').removeClass(arr_bg[i-1]);
				// $('.first-bg').fadeOut(function() {
				//     $(this).removeClass('brazil iceland cuba nepal japan thailand kenya');
				// });
				$('.first-bg').addClass(arr_bg[i]);
				// $('.first-bg').removeClass('fadeout fadein');
				// $('.first-bg').fadeIn(function() {
				//     $(this).addClass(arr_bg[i]);
				// });
			} else if(i%2) {
				$('.subfirst-bg').removeClass('brazil iceland cuba nepal japan thailand kenya');
				$('.first-bg').removeClass(arr_bg[i-1]);
				$('.subfirst-bg').addClass(arr_bg[i]);
			}
			$('.halls-country').removeClass('halls-brazil halls-iceland halls-cuba halls-nepal halls-japan halls-thailand halls-kenya');
			$('.halls-country').addClass(arr_country[i]);
			$('.fan-main-desk').removeClass('active');
		    $('.fan-main-desk').eq(i).addClass('active');
		    i++;
		    if(i >= 8) {
		    	i = 0;
		    	$('.first-bg').addClass('brazil');
		    	$('.first-bg').removeClass('iceland cuba nepal japan thailand kenya');
		    	$('.subfirst-bg').removeClass('brazil iceland cuba nepal japan thailand kenya');
		    	$('.halls-country').addClass('halls-brazil');
		    	$('.fan-main-desk').eq(0).addClass('active');
		    }
		}, 5000);
	}
	timeoutCountry();

});